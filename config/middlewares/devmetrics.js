/* globals require */
(function() {
  'use strict';

  module.exports = function(app, options) {
    //// CHECK already inited
    if (global.devmetrics) {
      console.log('no reinit, return cached');
      return global.devmetrics;
    }


    var token = options['token'];
    var host = options['host'];

    ///// LOGS
    var winston = require('winston'); require('winston-logstash');
    winston.emitErrs = true;
    var loggerObj = new winston.Logger({
        transports: [
            new winston.transports.Logstash({
                level: 'debug',
                port: 5545,
                node_name: token,
                host: host,
                handleExceptions: true
            }),
            new winston.transports.Console({
                level: 'debug',
                handleExceptions: true,
                json: false,
                colorize: true
            })
        ],
        exitOnError: false
    });

    loggerObj.info('devmetrics logger init');


    ///// STATSD SENDER
    var lynx = require('lynx');
    var metrics = new lynx(host, 5546, {scope:token});

    ///// REQUEST LOGS
    var logsStream  = {
        write: function(message, encoding){
            loggerObj.info(message);
        }
    };

    var morgan = require('morgan');
    morgan.token = morgan.token('statsdKey', function getStatsdKey(req) {
        return req.statsdKey
    });

    var requestLogHandler = morgan('{'+
        '"event_type":"morgan",' + 
        '"event_term":":status",' +
        '"event_key":":statsdKey",' + 
        '"session":":remote-addr",' +   /*-:remote-user*/
        '"request_uri":":statsdKey",' + 
        '"status_code":":status" ,' + 
        '"response_time":":response-time" ,' +
        '"val":"0" ,' + 
//      ' "message":"Test string"}'
 /*-:remote-user  VN 2015-04-04 Application crash in use remote-user */ 
        ' "message":"client - :remote-addr  [:date] :method :url HTTP/:http-version :status :res[content-length] :referrer :user-agent " }'
          , { 'stream': logsStream })

    var statsdURL = function (req, res, next) {
//        var method = req.method || 'unknown_method';
        req.statsdKey = ['http', req.path.replace(/[\/\.]/g, "-")].join('-');
        next();
      };

    ///// REQUEST METRICS
    var expressStatsd = require('express-statsd');
    var requestMetricHandler = expressStatsd({'client': metrics})

    if (app) {
      app.use(statsdURL);
      app.use(requestLogHandler);
      app.use(requestMetricHandler);
    }

    ///// Mongoose instrumentation
    function instrumentModel(obj) {
      for (var k in obj) {
        if (typeof(obj[k]) === 'function' && ['find', 'findById', 'findOne'].indexOf(k) > -1) {
          console.log('wrapping >>>>>>>', k, obj[k].name);
          if (k != obj[k].name) {
            return;
          }
          obj[k] = dmWrapModelFunction(obj[k]);
        }
      }
    }
//
//    function getParamNames(func) {
//      var funStr = func.toString();
//      return funStr.slice(funStr.indexOf('(')+1, funStr.indexOf(')')).match(/([^\s,]+)/g);
//    }
//
//    var getFunctionName = function (fn) {
//      return (fn + '').split(/\s|\(/)[1];
//    };

    var dmWrapModelFunction = function (fn, funcName) {
      return function () {
        console.log('>>>>> save start time');
        global.starttime = new Date().getTime();

        console.log('this.model.modelName', this.model.modelName);
        console.log('this.op', this.op);
        for (var i in arguments) {
          if (arguments[i] && typeof(arguments[i]) === 'function') {
            arguments[i] = dmMongooseInstrumentedExec(arguments[i], this.op, this.model.modelName);
            console.log('>>>>>>> instrumenting param', i, funcName);
          }
        }
//      console.log('diff', global.g_start - global.starttime);
        var res = fn.apply(this, arguments);
        var end = new Date().getTime();
        var duration = end - global.starttime;
//        console.log('>>>>>>>>>>>simple time', duration);
        return res;
      };
    };

    var dmMongooseInstrumentedExec = function (fn, funcName, modelName) {
      return function () {
        var res = fn.apply(this, arguments);
        var end = new Date().getTime();
        var duration = end - global.starttime;


        console.log(global.starttime, end, '>>>>>>>>>>>' + funcName + 'time', duration);
        var collectionName = modelName;
        var method = funcName;
        var query = '?';
        var doc = '?';
        var statsdKey = 'db--' + collectionName + '-' + method;

        loggerObj.log('debug', '{' +
          '"event_type":"mongoose", ' +
          '"event_key":"' + statsdKey + '", ' +
          '"event_term":"' + collectionName + '",' +
          '"session":"not-defined",' +
          '"request_uri":"' + statsdKey + '", ' +
          '"status_code":"0" ,' +
          '"response_time": "' + duration + '" ,' +
          '"val":"0" ,' +

          '"message":"db_request:' + collectionName + ' ' + method + ' ' + query + ' ' + doc + '"}'
        );


        return res;
      };
    };

    var dmExecWrapper = function(fn){
      return function(){
        arguments['0'] = dmMongooseInstrumentedExec(arguments['0']);
        return fn.apply(this, arguments);
      };
    };

//    var dmModelWrapper = function(fn){
//      return function() {
//        var origModel = fn.apply(this, arguments);
//        console.log('find check', origModel.find.name);
//        if (!origModel.find.name) {
//          console.log('>>>> already instrumented', arguments[0]);
//          return origModel;
//        }
//        if (arguments[0] == 'Package') {
//          console.log('>>>> skipping model', arguments[0]);
//          return origModel;
//        } else {
//          console.log('>>>> instrumenting model', arguments[0]);
//        }
////        console.log('>>>> instrumenting model', arguments[0]);
//        return instrumentModel(origModel);
//      };
//    };

    var mongoose = require('mongoose');
    console.log('>>>>>>>>>>>>>>>> wrapping exec');
//    mongoose.Query.prototype.exec = dmExecWrapper(mongoose.Query.prototype.exec, 'exec');
    mongoose.Query.prototype.findOne = dmWrapModelFunction(mongoose.Query.prototype.findOne, 'findOne');
    mongoose.Query.prototype.find = dmWrapModelFunction(mongoose.Query.prototype.find, 'find');
//    mongoose.model = dmModelWrapper(mongoose.model);

    global.devmetrics = {'logger': loggerObj, 'metrics': metrics, 'requestLogs': requestLogHandler,
      'requestMetrics': requestMetricHandler, 'instrumentModel': instrumentModel};
    return global.devmetrics;
  }

//
//    var mongoose = require('mongoose');
//    loggerObj.info('mongoose devmetrics request init');
//
//    mongoose.set('debug', function (collectionName, method, query, doc, options) {
//      //loggerObj.info('mongoose devmetrics request debug');
//
//      var statsdKey = 'db--' + collectionName + '-' + method;
//
//      loggerObj.log('debug', '{' +
//        '"event_type":"mongoose", ' +
//        '"event_key":"' + statsdKey + '", ' +
//        '"event_term":"' + collectionName + '",' +
//        '"session":"not-defined",' +
//        '"request_uri":"' + statsdKey + '", ' +
//        '"status_code":"0" ,' +
//        '"response_time":"0" ,' +
//        '"val":"0" ,' +
//
//        '"message":"db_request:' + collectionName + ' ' + method + ' ' + query + ' ' + doc + '"}'
//      );
//    });



})();
